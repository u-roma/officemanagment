# System of rooms and staff management

## test project for using:

	* Spring Security
	* Hibernate
	* MySql
	* Java8
	* bootstrap
	* gradle
	* AOP
	
## Run the project.

to run the project with gradle use command:

on widows:
```
gradlew jettyRun
```

on linux:
```
./gradlew jettyRun
```

Also db can be configured through parameters:
h2 (default):
```
gradlew clean jettyRun  -PdbType=h2
``` 

mysql:
```
gradlew clean jettyRun  -PdbType=mysql
``` 

since db properties are copied diring the build stage, they can be cached. So make sure you use "clean" command 
with dbType parameter.

##Debug project
Official documentation says that adding  **'-Dorg.gradle.debug=true'** is enough. But it does not work for me.
What work for me is ( [link](http://stackoverflow.com/questions/9315398/how-to-run-jetty-via-gradle-in-debug-mode) ):  
windows:
```
gradlew -Dorg.gradle.jvmargs="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n" jettyRun
```

linux:
```
./gradlew -Dorg.gradle.jvmargs="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n" jettyRun
```
 
 After that it is possible to attach debugger on port 8000


## Using Sonarqube in the project.

First, you need to have Docker installed on your machine.

Run sonarqube in the docker container:

```
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:5.2
```

After that, run code analize:

```
 gradle sonarqube -Dsonar.host.url="http://[ip_of_running_docker_container]:9000" -Dsonar.host.url="jdbc:h2:tcp://[ip_of_running_docker_container]/sonar"
```

for example for Docker toolbox on windows/maxOs

```
 gradle sonarqube -Dsonar.host.url="http://192.168.99.100:9000" -Dsonar.host.url="jdbc:h2:tcp://192.168.99.100/sonar"
```

or for linux

```
gradle sonarqube -Dsonar.host.url="http://localhost:9000" -Dsonar.host.url="jdbc:h2:tcp://localhost/sonar"
```

more info:
https://plugins.gradle.org/plugin/org.sonarqube
http://docs.sonarqube.org/display/SONAR/Analyzing+with+SonarQube+Scanner+for+Gradle

## TODO list

 * Spring Security (acl, base authentication, OpenId, OAuth2)
 * Распределенные транзации Spring MVC
 * AOP (Spring AOP) for (probably) logging goals.