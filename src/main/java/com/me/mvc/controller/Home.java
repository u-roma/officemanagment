package com.me.mvc.controller;

import java.text.MessageFormat;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.me.mvc.service.ClientService;
import com.me.mvc.service.LogService;
import com.me.mvc.service.SystemPropertyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class Home {

    @Autowired
    private ClientService clientService;

    @Autowired
    private SystemPropertyService systemPropertyService;

    @Autowired
    private LogService logService;

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world! root");
        clientService.getPage(1);
        return "index";
    }

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String printWelcome1(ModelMap model, HttpServletRequest request) {

        model.addAttribute("message", "Hello world! index");
        return "index";
    }

    @RequestMapping(value = "contacts", method = RequestMethod.GET)
    public String contacts(ModelMap model, HttpServletRequest request) {

        //todo this should be used by AOP on any controller mwthods.
        logService.logSystemInfo(MessageFormat.format("{0} visit contacts, with cookies {1}",
                getHeaders(request),
                getCookies(request)));

        return "contacts";
    }

    @RequestMapping(value = "about", method = RequestMethod.GET)
    public String about(ModelMap model, HttpServletRequest request) {

        logService.logSystemInfo(MessageFormat.format("{0} visit contacts, with cookies {1}",
                getHeaders(request),
                getCookies(request)));

        return "about";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String logout(ModelMap model, HttpServletRequest request) {

        logService.logSystemInfo(MessageFormat.format("{0} visit contacts, with cookies {1}",
                getHeaders(request),
                getCookies(request)));

        return "login";
    }

    //todo this should be used by AOP on any controller mwthods.
    private String getCookies(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();
        if (request.getCookies() != null) {
            final Cookie[] cookies = request.getCookies();
            for (Cookie cooky : cookies) {
                stringBuilder.append(cooky.getName() + "=" + cooky.getValue() + "; ");
            }
        }
        return stringBuilder.toString();
    }

    //todo this should be used by AOP on any controller mwthods.
    private String getHeaders(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();
        final Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement().toString();
            stringBuilder.append(request.getHeader(headerName) + "; ");
        }
        return stringBuilder.toString();
    }

}