package com.me.mvc.controller.tables;

import com.me.mvc.persistence.model.entity.ClientGroup;
import com.me.mvc.service.ClientGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/clientGroups")
public class ClientGroups {

    @Autowired
    private ClientGroupService clientGroupService;

    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model,  Pageable pageable) {

        model.put("newClientGroup", new ClientGroup());
        model.addAttribute("clientGroups", (Page) clientGroupService.getPage(pageable));
        return "tables/clientGroups";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @ModelAttribute("newClientGroup") ClientGroup clientGroup,
                               Pageable pageable) {

        clientGroupService.saveEntity(clientGroup);
        model.put("newClientGroup", new ClientGroup());
        model.addAttribute("clientGroups", (Page) clientGroupService.getPage(pageable));
        return "tables/clientGroups";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{clientGroupId}")
    public String propertyGet(ModelMap model, @PathVariable int clientGroupId, Pageable pageable) {

        model.addAttribute("newClientGroup", clientGroupService.getEntity(clientGroupId));
        model.addAttribute("clientGroups", (Page) clientGroupService.getPage(pageable));
        return "tables/clientGroups";
    }
}
