package com.me.mvc.controller.tables;

import com.me.mvc.persistence.bind.MyEntityBinder;
import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.persistence.model.entity.ClientGroup;
import com.me.mvc.persistence.model.entity.ClientInGroup;
import com.me.mvc.service.ClientGroupService;
import com.me.mvc.service.ClientInGroupService;
import com.me.mvc.service.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/clientInGroup")
public class ClientInGroups {

    @Autowired
    private ClientGroupService clientGroupService;

    @Autowired
    private ClientInGroupService clientInGroupService;

    @Autowired
    private ClientService clientService;

    @InitBinder
    protected void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Client.class, "client", new MyEntityBinder(clientService));
        binder.registerCustomEditor(ClientGroup.class, "clientGroup", new MyEntityBinder(clientGroupService));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model, Pageable pageable) {

        model.put("newClientInGroup", new ClientInGroup());
        model.addAttribute("clientInGroupPage", (Page) clientInGroupService.getPage(pageable));
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("clientGroups", clientGroupService.getAll());
        return "tables/clientInGroup";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @ModelAttribute("newClientGroup") ClientInGroup clientInGroup,
                               BindingResult result, Pageable pageable) {

        result.toString();
        clientInGroupService.saveEntity(clientInGroup);
        model.put("newClientInGroup", new ClientInGroup());
        model.addAttribute("clientInGroupPage", (Page) clientInGroupService.getPage(pageable));
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("clientGroups", clientGroupService.getAll());
        return "tables/clientInGroup";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{clientGroupId}")
    public String propertyGet(ModelMap model, @PathVariable int clientGroupId, Pageable pageable) {

        model.addAttribute("newClientInGroup", clientInGroupService.getEntity(clientGroupId));
        model.addAttribute("clientInGroupPage", (Page) clientInGroupService.getPage(pageable));
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("clientGroups", clientGroupService.getAll());
        return "tables/clientInGroup";
    }
}
