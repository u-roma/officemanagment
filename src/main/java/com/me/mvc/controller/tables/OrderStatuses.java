package com.me.mvc.controller.tables;

import com.me.mvc.persistence.model.entity.OrderStatus;
import com.me.mvc.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/orderStatuses")
public class OrderStatuses {

    @Autowired
    private OrderStatusService orderStatusService;

    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model,  Pageable pageable) {

        model.put("newOrderStatus", new OrderStatus());
        model.addAttribute("orderStatuses", (Page) orderStatusService.getPage(pageable));
        return "tables/orderStatuses";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @ModelAttribute("newOrderStatus") OrderStatus orderStatus,
                               Pageable pageable) {

        orderStatusService.saveEntity(orderStatus);
        model.put("newOrderStatus", new OrderStatus());
        model.addAttribute("orderStatuses", (Page) orderStatusService.getPage(pageable));
        return "tables/orderStatuses";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{orderStatusId}")
    public String propertyGet(ModelMap model, @PathVariable int orderStatusId, Pageable pageable) {

        model.addAttribute("newOrderStatus", orderStatusService.getEntity(orderStatusId));
        model.addAttribute("orderStatuses", (Page) orderStatusService.getPage(pageable));
        return "tables/orderStatuses";
    }
}
