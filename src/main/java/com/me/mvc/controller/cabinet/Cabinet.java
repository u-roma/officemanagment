package com.me.mvc.controller.cabinet;

import com.me.mvc.persistence.bind.MyEntityBinder;
import com.me.mvc.persistence.model.entity.*;
import com.me.mvc.service.BusinessOrderService;
import com.me.mvc.service.ClientService;
import com.me.mvc.service.OrderStatusService;
import com.me.mvc.service.RoomService;
import com.me.mvc.service.StaffService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Roman Uholnikov
 */
@Controller
@RequestMapping("/cabinet")
public class Cabinet {

    @Autowired
    private BusinessOrderService orderService;

    @Autowired
    private OrderStatusService orderStatusService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private StaffService staffService;

    @Autowired
    private RoomService roomService;

    @InitBinder
    protected void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Client.class, "client", new MyEntityBinder(clientService));
        binder.registerCustomEditor(Staff.class, "staff", new MyEntityBinder(staffService));
        binder.registerCustomEditor(OrderStatus.class, "orderStatus", new MyEntityBinder(orderStatusService));
        binder.registerCustomEditor(Room.class, "room", new MyEntityBinder(roomService));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world!");
        return "cabinet/index";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String ordersListGet(ModelMap model, Pageable pageable) {
        model.addAttribute("orders", (Page) orderService.getPage(pageable));
        return "cabinet/orders";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public String ordersListPost(ModelMap model, Pageable pageable,
                                 @ModelAttribute("order") BusinessOrder businessOrder) {
        //todo add validation
        orderService.saveEntity(businessOrder);
        Page ordersPage = (Page) orderService.getPage(pageable);
        model.addAttribute("orders", ordersPage);
        return "cabinet/orders";
    }

    @RequestMapping(value = "/orders/{orderId}", method = RequestMethod.GET)
    public String order(ModelMap model, @PathVariable("orderId") int orderId) {
        setUpModel(model, orderService.getEntity(orderId));
        return "cabinet/order";
    }

    @RequestMapping(value = "/orders/newOrder", method = RequestMethod.GET)
    public String newOrder(ModelMap model) {
        final BusinessOrder businessOrder = new BusinessOrder();
        setUpModel(model, businessOrder);
        return "cabinet/order";
    }

    private void setUpModel(final ModelMap model, final BusinessOrder businessOrder) {
        model.addAttribute("order", businessOrder);
        model.addAttribute("clients", clientService.getAll());
        model.addAttribute("staffs", staffService.getAll());
        model.addAttribute("rooms", roomService.getAll());
        model.addAttribute("orderStatuses", orderStatusService.getAll());
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        model.addAttribute("dateTimeFormatter", dateTimeFormatter);
    }
}
