package com.me.mvc.persistence.model.entity;

import java.text.MessageFormat;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Just abstract entity
 *
 * @author Roman Uholnikov
 */
@MappedSuperclass
public class AbstractEntity {

    public static final int TO_STRING_LIMIT = 10;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AbstractEntity() {
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} ({1})", getTitle(),
                getDescription().substring(0, getDescription().length() > TO_STRING_LIMIT ? TO_STRING_LIMIT : getDescription().length()));
    }
}
