package com.me.mvc.persistence.model.entity.representation.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.me.mvc.persistence.model.entity.BusinessOrder;
import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.persistence.model.entity.Room;
import com.me.mvc.persistence.model.entity.Staff;

/**
 * @author Roman Uholnikov
 */
@XmlRootElement(name = "list")
@XmlSeeAlso({Client.class, BusinessOrder.class, Room.class, Staff.class})
public class EntityList<T> {

    private List<T> listOfEntityObjects;

    public EntityList() {
        listOfEntityObjects = new ArrayList<T>();
    }

    public EntityList(List<T> listOfEntityObjects) {
        this.listOfEntityObjects = listOfEntityObjects;
    }

    @XmlAnyElement
    public List<T> getItems() {
        return listOfEntityObjects;
    }

}
