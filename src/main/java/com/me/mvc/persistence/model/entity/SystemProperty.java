package com.me.mvc.persistence.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Roman Uholnikov
 */
@Entity
public class SystemProperty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name="keyName", unique = true)
    private String key;

    @NotNull
    @Column(name="valueValue")
    private String value;

    public SystemProperty() {
    }

    public SystemProperty(final int id) {
        this.id = id;
    }

    public SystemProperty(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SystemProperty)) {
            return false;
        }

        final SystemProperty that = (SystemProperty) o;

        if (getId() != that.getId()) {
            return false;
        }
        if (!getKey().equals(that.getKey())) {
            return false;
        }
        return getValue().equals(that.getValue());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getKey() != null ? getKey().hashCode() : 0);
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }
}
