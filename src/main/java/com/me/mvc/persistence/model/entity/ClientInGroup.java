package com.me.mvc.persistence.model.entity;

import javax.persistence.*;

/**
 * Interdependency item between {@link com.me.mvc.persistence.model.entity.Client} and {@link
 * com.me.mvc.persistence.model.entity.ClientInGroup}
 *
 * @author Roman Uholnikov
 */
@Entity
public class ClientInGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(targetEntity = Client.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, optional = false)
    private Client client;


    @ManyToOne(targetEntity = ClientGroup.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, optional = false)
    private ClientGroup clientGroup;

    public ClientInGroup() {
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(final Client client) {
        this.client = client;
    }

    public ClientGroup getClientGroup() {
        return clientGroup;
    }

    public void setClientGroup(final ClientGroup clientGroup) {
        this.clientGroup = clientGroup;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ClientInGroup that = (ClientInGroup) o;

        if (id != that.id) {
            return false;
        }
        if (!client.equals(that.client)) {
            return false;
        }
        if (!clientGroup.equals(that.clientGroup)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + client.hashCode();
        result = 31 * result + clientGroup.hashCode();
        return result;
    }
}
