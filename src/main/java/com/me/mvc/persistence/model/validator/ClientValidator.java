package com.me.mvc.persistence.model.validator;

import com.me.mvc.persistence.model.entity.Client;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Roman Uholnikov
 */
@Component
public class ClientValidator implements Validator {

    @Override
    public boolean supports(final Class<?> aClass) {
        return Client.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {
        //check all fields and that such user does not exist yet
    }
}
