package com.me.mvc.persistence.model.entity;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Roman Uholnikov
 */
@Entity
public class BusinessOrder {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime creationTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @ManyToOne(targetEntity = OrderStatus.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private OrderStatus orderStatus;
    @ManyToOne(targetEntity = Room.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Room room;
    @ManyToOne(targetEntity = Staff.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Staff staff;
    @ManyToOne(targetEntity = Client.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Client client;

    @Type(type = "text")
    private String details;

    public BusinessOrder() {
        this.creationTime = LocalDateTime.now();
        this.startTime = LocalDateTime.now();
        this.endTime = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BusinessOrder that = (BusinessOrder) o;

        if (id != that.id) {
            return false;
        }
        if (client != null ? !client.equals(that.client) : that.client != null) {
            return false;
        }
        if (creationTime != null ? !creationTime.equals(that.creationTime) : that.creationTime != null) {
            return false;
        }
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
            return false;
        }
        if (orderStatus != null ? !orderStatus.equals(that.orderStatus) : that.orderStatus != null) {
            return false;
        }
        if (room != null ? !room.equals(that.room) : that.room != null) {
            return false;
        }
        if (staff != null ? !staff.equals(that.staff) : that.staff != null) {
            return false;
        }
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (creationTime != null ? creationTime.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (orderStatus != null ? orderStatus.hashCode() : 0);
        result = 31 * result + (room != null ? room.hashCode() : 0);
        result = 31 * result + (staff != null ? staff.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        return result;
    }
}
