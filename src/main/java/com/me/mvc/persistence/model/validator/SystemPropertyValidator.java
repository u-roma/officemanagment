package com.me.mvc.persistence.model.validator;

import com.me.mvc.persistence.model.entity.SystemProperty;
import com.me.mvc.persistence.repository.SystemPropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * @author Roman Uholnikov
 */
@Component
public class SystemPropertyValidator implements Validator {

    @Autowired
    private SystemPropertyRepository systemPropertyRepository;

    @Override
    public boolean supports(final Class<?> aClass) {
        return SystemProperty.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {

        SystemProperty systemProperty = (SystemProperty) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "key", "Empty key field");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "Empty value field");

        if(!systemProperty.getKey().matches("[a-zA-Z0-9]+")){
            errors.rejectValue("key", "Key Can contains just numbers and symbols!");
        }

        List<SystemProperty> existedList = systemPropertyRepository.findByKeyLike(systemProperty.getKey());

        if(!existedList.isEmpty()
                && !existedList.stream().anyMatch(p -> systemProperty.getId() == p.getId())){
            errors.rejectValue("key", "This key already exist!");
        }
    }
}
