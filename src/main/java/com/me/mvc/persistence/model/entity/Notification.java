package com.me.mvc.persistence.model.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Roman Uholnikov
 */
@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private LocalDateTime creationTime;
    private LocalDateTime notificationTime;
    private String message;
    private String info;
    private String emails;
    @ManyToOne(targetEntity = BusinessOrder.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private BusinessOrder businessOrder;


    public Notification() {
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(final LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(final LocalDateTime notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(final String info) {
        this.info = info;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(final String emails) {
        this.emails = emails;
    }

    public BusinessOrder getBusinessOrder() {
        return businessOrder;
    }

    public void setBusinessOrder(final BusinessOrder businessOrder) {
        this.businessOrder = businessOrder;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Notification that = (Notification) o;

        if (id != that.id) {
            return false;
        }
        if (creationTime != null ? !creationTime.equals(that.creationTime) : that.creationTime != null) {
            return false;
        }
        if (emails != null ? !emails.equals(that.emails) : that.emails != null) {
            return false;
        }
        if (info != null ? !info.equals(that.info) : that.info != null) {
            return false;
        }
        if (message != null ? !message.equals(that.message) : that.message != null) {
            return false;
        }
        if (notificationTime != null ? !notificationTime.equals(that.notificationTime) : that.notificationTime != null) {
            return false;
        }
        if (businessOrder != null ? !businessOrder.equals(that.businessOrder) : that.businessOrder != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (creationTime != null ? creationTime.hashCode() : 0);
        result = 31 * result + (notificationTime != null ? notificationTime.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (emails != null ? emails.hashCode() : 0);
        result = 31 * result + (businessOrder != null ? businessOrder.hashCode() : 0);
        return result;
    }
}
