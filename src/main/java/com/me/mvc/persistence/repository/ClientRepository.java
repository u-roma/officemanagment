package com.me.mvc.persistence.repository;

import com.me.mvc.persistence.model.entity.Client;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roman Uholnikov
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {


    /**
     * Find persons like first name.
     */
    public Page<Client> findByNameLike(String firstName, Pageable pageable);

    /**
     * Find persons by last name.
     */
    public Page<Client> findByName(String lastName, Pageable pageable);

    /**
     * Find persons by address.
     */
//    todo
//    public final static String FIND_BY_ADDRESS_QUERY = "SELECT p " +
//            "FROM Person p LEFT JOIN p.addresses a " +
//            "WHERE a.address = :address";
//
//    @Query(FIND_BY_ADDRESS_QUERY)
//    public List<Client> findByAddress(@Param("address") String address);

}
