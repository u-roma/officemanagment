package com.me.mvc.persistence.repository;

import com.me.mvc.persistence.model.entity.Room;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roman Uholnikov
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    public Page<Room> findByTitleLike(String firstName, Pageable pageable);
}
