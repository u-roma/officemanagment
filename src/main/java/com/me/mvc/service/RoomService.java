package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.Room;
import com.me.mvc.persistence.repository.RoomRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class RoomService extends AbstractEntityService<RoomRepository, Room> {

}
