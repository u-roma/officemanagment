package com.me.mvc.service;

import java.util.LinkedList;
import java.util.List;

import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.persistence.repository.ClientRepository;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class ClientService extends AbstractEntityService<ClientRepository, Client> {

    public Page<Client> getByNameLikePaged(String fragmentOfName, int pageIndex) {
        List<Client> result = new LinkedList<>();
        return this.getRepository().findByNameLike("%" + fragmentOfName + "%", constructPageSpecification(pageIndex));
    }


}
