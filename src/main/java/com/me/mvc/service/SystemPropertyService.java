package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.SystemProperty;
import com.me.mvc.persistence.repository.SystemPropertyRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class SystemPropertyService extends AbstractEntityService<SystemPropertyRepository, SystemProperty> {

}
