package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.persistence.model.entity.ClientGroup;
import com.me.mvc.persistence.repository.ClientGroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class ClientGroupService extends AbstractEntityService<ClientGroupRepository, ClientGroup> {

    public Page<ClientGroup> getByTitleLikePaged(String title, int pageIndex) {
        List<Client> result = new LinkedList<>();
        return this.getRepository().findByTitleLike("%" + title + "%", constructPageSpecification(pageIndex));
    }


    public Page<ClientGroup> getByAdditionalInfoLikePaged(String additionalInfo, int pageIndex) {
        List<Client> result = new LinkedList<>();
        return this.getRepository().findByTitleLike("%" + additionalInfo + "%", constructPageSpecification(pageIndex));
    }


}
