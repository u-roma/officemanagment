package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.Log;
import com.me.mvc.persistence.repository.LogRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class LogService extends AbstractEntityService<LogRepository, Log> {

    public void logSystemInfo(String message){
        getRepository().save(new Log(Log.SYSTEM_TYPE, Log.INFO_LEVEL, message));
    }

    public void logSystemWarn(String message){
        getRepository().save(new Log(Log.SYSTEM_TYPE, Log.WARN_LEVEL, message));
    }

    public void logSystemError(String message){
        getRepository().save(new Log(Log.BUSINESS_TYPE, Log.ERROR_LEVEL, message));
    }

    public void logBusinessInfo(String message){
        getRepository().save(new Log(Log.BUSINESS_TYPE, Log.INFO_LEVEL, message));
    }

    public void logBusinessmWarn(String message){
        getRepository().save(new Log(Log.BUSINESS_TYPE, Log.WARN_LEVEL, message));
    }

    public void logBusinessError(String message){
        getRepository().save(new Log(Log.BUSINESS_TYPE, Log.ERROR_LEVEL, message));
    }

}
