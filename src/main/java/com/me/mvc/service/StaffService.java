package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.Staff;
import com.me.mvc.persistence.repository.StaffRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class StaffService extends AbstractEntityService<StaffRepository, Staff> {


}
