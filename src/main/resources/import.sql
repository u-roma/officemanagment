/*# it is initial data for Data Base
# SystemProperty table
*/

/*log table table*/
INSERT INTO `officemanagement_03`.`log` (`id`, `level`, `message`) VALUES ('0', '1', 'start system');
/*
-- Query: SELECT * FROM officemanagement_03.SystemProperty
LIMIT 0, 1000

-- Date: 2015-12-18 17:41
*/
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (1,'clinic.name','MyClinic');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (2,'google.calendar.oauth.client.id','348215374128-qffr7jd5j8o7ft2i988v4ufptvae1qiu.apps.googleusercontent.com');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (3,'google.calendar.oauth.client.secret','ESkX7xIrYJ4aOiADNi648k65');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (6,'dd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (7,'asd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (8,'aaa','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (9,'dddd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (10,'sdsdsd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (11,'asdasdd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (12,'nnn','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (13,';','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (14,'\\','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (15,'vb','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (16,'gfg','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (17,'dfff','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (18,'sfd','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (19,'j','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (20,'jjj','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (21,'jj','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (23,'а','');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (24,'ывафывавыа','ывавыа');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (25,'ыва','ыва');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (26,'sdfff','ffs');
INSERT INTO `SystemProperty` (`id`,`keyName`,`valueValue`) VALUES (27,'xe','vdwf');

INSERT INTO `OrderStatus` (`id`,`title`,`description`) VALUES (1,'created','Just newly created order');
INSERT INTO `OrderStatus` (`id`,`title`,`description`) VALUES (2,'to be aligned','Time or any other details should be aligned.');
INSERT INTO `OrderStatus` (`id`,`title`,`description`) VALUES (3,'aligned','Everything was aligned successfully. Ready for serving. ');
INSERT INTO `OrderStatus` (`id`,`title`,`description`) VALUES (4,'served','Order that was served successfully');
INSERT INTO `OrderStatus` (`id`,`title`,`description`) VALUES (5,'canceled','Canceled by any reason order');

INSERT INTO `Room` (`id`,`title`,`description`) VALUES (1,'11','Ренген кабинет');
INSERT INTO `Room` (`id`,`title`,`description`) VALUES (2,'12','');
INSERT INTO `Room` (`id`,`title`,`description`) VALUES (3,'13','');
INSERT INTO `Room` (`id`,`title`,`description`) VALUES (4,'14','');
INSERT INTO `Room` (`id`,`title`,`description`) VALUES (5,'16','кабинет дизинфекции');
INSERT INTO `Room` (`id`,`title`,`description`) VALUES (6,'103','Херургия');

INSERT INTO `ClientGroup` (`id`,`title`,`description`) VALUES (1,'Депутаты','');
INSERT INTO `ClientGroup` (`id`,`title`,`description`) VALUES (2,'Семья Фроловых',' Важно, приближенные начальства');
INSERT INTO `ClientGroup` (`id`,`title`,`description`) VALUES (3,'Льготники: пинсионеры',' Скидка 20%');

INSERT INTO `Client` (`id`, `additionalInfo`, `contacts`, `email`, `login`, `name`, `passwordHash`, `address`, `birthday`) VALUES ('2', '', '', 'none@none', 'Julia', 'Julia Loboda', '4a7d1ed414474e4033ac29ccb8653d9b', 'Киев', '1990-12-19');
INSERT INTO `Client` (`id`, `additionalInfo`, `contacts`, `email`, `login`, `name`, `passwordHash`, `address`, `birthday`) VALUES ('1', 'много работает', '0668801524', 'ugolnikovroman@gmail.com', 'roman_client', 'Roman Uholnikov', '4a7d1ed414474e4033ac29ccb8653d9b', 'Киев', '1989-12-20');

INSERT INTO `Staff` (`id`, `additionalInfo`, `contacts`, `email`, `login`, `name`, `passwordHash`, `address`, `birthday`) VALUES ('1', 'много работает', '0668801524', 'ugolnikovroman@gmail.com', 'roman', 'Roman Uholnikov', '4a7d1ed414474e4033ac29ccb8653d9b', 'Киев', '1989-12-20');
INSERT INTO `Staff` (`id`, `additionalInfo`, `contacts`, `email`, `login`, `name`, `passwordHash`, `address`, `birthday`) VALUES ('2', '', '', 'ugolnikovroman@gmail.com', 'ivan', 'Ivan ', '4a7d1ed414474e4033ac29ccb8653d9b', 'Киев', '1989-12-20');

INSERT INTO `BusinessOrder` (`id`, `creationTime`, `details`, `endTime`, `startTime`, `client_id`, `orderStatus_id`, `room_id`, `staff_id`) VALUES ('1', '2016-02-20 16:11:00', 'some details', '2016-02-20 16:11:00', '2016-02-20 16:11:00', '1', '1', '1', '2');
